package edu.wit.hbasemanager.common.result;

import lombok.Data;

@Data
public class Status {
    private String msg;
    private Integer code;
}