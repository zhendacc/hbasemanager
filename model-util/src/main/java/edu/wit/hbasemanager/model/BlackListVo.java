package edu.wit.hbasemanager.model;

import lombok.Data;

@Data
public class BlackListVo {
    private String email;
    private boolean ban;

    public BlackListVo(String email, boolean ban) {
        this.email = email;
        this.ban = ban;
    }
}
