package edu.wit.hbasemanager.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Classname SourceInfo
 * @Description TODO
 * @Author Shawn Yue
 * @Date 22:13
 * @Version 1.0
 **/
@Data
public class SourceInfo implements Serializable {
    private final static Long serialVersionUID = 12345L;
    private String name;
    private String value;

    public SourceInfo(String name, String value) {
        this.name = name;
        this.value = value;
    }
    public SourceInfo(){

    }
}
