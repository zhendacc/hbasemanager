package edu.wit.hbasemanager.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class MailMessage implements Serializable {
    private final static long serialVersionUID = 12345L;
    private String targetMail;
    private String message;
    private String subject;
}
