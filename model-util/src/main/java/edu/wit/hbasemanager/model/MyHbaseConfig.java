package edu.wit.hbasemanager.model;

import lombok.Data;
import org.apache.hadoop.conf.Configuration;

import java.io.Serializable;
@Data
public class MyHbaseConfig implements Serializable {
    private static final Long serialVersionUID = 123L;
    private Configuration configuration;
}
