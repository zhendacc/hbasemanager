package edu.wit.hbasemanager.user.service;

import edu.wit.hbasemanager.model.BlackListVo;
import edu.wit.hbasemanager.model.LoginVo;

import java.util.List;

public interface UserService {
   LoginVo loginService(String email, String code);
   String logoutService(String email);
   Boolean blackListService(String mail);

   List<BlackListVo> getBlackListService(String email);
   void sendCodeService(String email, String code);
}
