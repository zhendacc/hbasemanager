package edu.wit.hbasemanager.user.controller;

import com.alibaba.fastjson.JSONObject;
import edu.wit.hbasemanager.common.code.VerfiyCodeUtil;
import edu.wit.hbasemanager.common.result.Result;
import edu.wit.hbasemanager.model.BlackListVo;
import edu.wit.hbasemanager.model.LoginVo;
import edu.wit.hbasemanager.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;


    /**
     * 用户登录
     * @return Result
     */
    @PostMapping("/login")
    public Result login(@RequestBody JSONObject json) {
        String email = json.getString("email");
        String code = json.getString("code");
        if (email==null || code==null) {
            //错误
            return Result.fail();
        }
        LoginVo loginVo = userService.loginService(email, code);
        if (loginVo!=null) {
            return Result.ok(loginVo);
        } else {
            return Result.fail();
        }

    }

    /**
     * 用户登出
     * @param email
     * @return
     */
    @PostMapping("/auth/logout")
    public Result logout(@RequestHeader("email") String email) {
        if (email==null) {
            //错误
            return Result.fail();
        }
        userService.logoutService(email);
        return Result.ok();
    }

    /**
     * 获取用户黑名单
     * @param email
     * @return
     */
    @GetMapping("auth/getBlackList")
    public Result getBlackList(@RequestHeader("email") String email) {
        if (email==null) {
            return Result.fail();
        }
        List<BlackListVo> blackList = userService.getBlackListService(email);
        return Result.ok(blackList);
    }
    /**
     *  黑名单
     * @param email
     * @return
     */
    @PostMapping("auth/blackList")
    public Result blackList(@RequestHeader("email") String email) {
        if (email==null) {
            return Result.fail();
        }
        boolean ban = userService.blackListService(email);
        if (ban) {
            return Result.build(200,"用户已禁用");
        } else {
            return Result.build(200,"用户已启用");
        }

    }

    /**
     * 发送验证码
     * @param json
     * @return
     */
    @PostMapping("/sendCode")
    public Result sendCode(@RequestBody JSONObject json) {
        String email = json.getString("email");
        log.info("email:{}",email);
        //生成6位验证码，放入redis,
        String code = VerfiyCodeUtil.generateCode();
        userService.sendCodeService(email,code);
        return Result.ok();
    }
}
