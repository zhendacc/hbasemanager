package edu.wit.hbasemanager.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Getter
@Setter
@TableName("hbase_user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId("id")
    private long userId;
    @TableField("email")
    private String email;
    @TableField("is_admin")
    private boolean admin;
}
