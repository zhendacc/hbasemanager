package edu.wit.hbasemanager.controller;

import com.alibaba.fastjson.JSONObject;
import edu.wit.hbasemanager.model.ConnectionManager;
import edu.wit.hbasemanager.model.MyHbaseConfig;
import edu.wit.hbasemanager.model.SourceInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/db/connection/inner/")
public class ConnectionController {
    @GetMapping("getConnection/{name}")
    public Connection getConnection(@PathVariable("name") String name) {
        return ConnectionManager.getConnection(name);
    }
    @GetMapping("closeConnection/{name}")
    public boolean closeConnection(@PathVariable("name") String name) {
        return ConnectionManager.closeConnection(name);
    }
    @PostMapping("createConnection")
    public Connection CreateConnection(@RequestParam("name") String name, @RequestBody List<SourceInfo> list) {
        log.info("创建连接。。。");
       Configuration config = new Configuration();
       for (SourceInfo info:list) {
           config.set(info.getName(),info.getValue());
       }
       return ConnectionManager.createConnection(name,config);
    }
}
