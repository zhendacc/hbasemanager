package edu.wit.hbasemanager.manage.manageservice.HBaseTool;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import edu.wit.hbasemanager.manage.manageservice.service.HbaseDataSourceConfigurationService;
import edu.wit.hbasemanager.manage.manageservice.service.HbaseManageService;
import edu.wit.hbasemanager.manage.manageservice.serviceImpl.HbaseManageServiceImpl;
import edu.wit.hbasemanager.model.SourceInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Slf4j
@Component
public class ConnectionGet {
    @Autowired
    HbaseManageService hbaseManageService;
    @Autowired
    HbaseDataSourceConfigurationService hdsService;

    public Connection getConnectionFromSql (String sourceName){
        String s = hdsService.selectPropertyBySourceName(sourceName);
        Configuration configuration = new Configuration();
        List<SourceInfo> sourceInfoList = JSONObject.parseArray(s, SourceInfo.class);
        log.info(sourceInfoList.toString());
        for (SourceInfo si : sourceInfoList){
            configuration.set(si.getName(), si.getValue());
        }
        return ConnectionManager.createConnection(sourceName ,configuration);
    }
}
