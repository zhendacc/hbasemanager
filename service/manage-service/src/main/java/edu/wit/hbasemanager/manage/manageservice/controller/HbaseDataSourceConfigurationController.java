package edu.wit.hbasemanager.manage.manageservice.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import edu.wit.hbasemanager.common.result.Result;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.ConnectionGet;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.ConnectionManager;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.HBaseDDL;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.HBaseDML;
import edu.wit.hbasemanager.manage.manageservice.feign.DbFeignClient;
import edu.wit.hbasemanager.manage.manageservice.service.HbaseDataSourceConfigurationService;
import edu.wit.hbasemanager.manage.manageservice.service.HbaseManageService;
import edu.wit.hbasemanager.manage.manageservice.serviceImpl.HbaseManageServiceImpl;
import edu.wit.hbasemanager.model.MyHbaseConfig;
import edu.wit.hbasemanager.model.SourceInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.wit.hbasemanager.common.result.ResultCodeEnum.DATA_ERROR;
import static edu.wit.hbasemanager.common.result.ResultCodeEnum.PERMISSION;

/**
 * @Author Shawn Yue
 * @Description //TODO Shawn Yue
 * @Date 18:04 2022/9/26
 * @Param
 * @return
 **/
@RestController
@Slf4j
@RequestMapping("/api/manage/hbaseDataSourceConfiguration")
public class HbaseDataSourceConfigurationController {
    @Autowired
    HbaseManageService hbaseManageService;
    @Autowired
    HbaseDataSourceConfigurationService hdsService;
    @Autowired
    ConnectionGet get;


    /**
     * @Author Shawn Yue
     * @Description // 创建新的数据源
     *              // 在mysql管理表中添加新的数据源信息
     *              // 更新mysql数据源表
     * @Date 15:56 2022/9/27
     * @Param [sourceName, configMap, userId]
     **/
    @PostMapping("/auth/createDataConfig")
    public Result createDataConfig(@RequestBody JSONObject sourceInfo,
                                   @RequestHeader("userId") Integer userId,
                                   @RequestHeader("role") String role){
        if(role.equals("user")){
            return Result.build(null, PERMISSION);
        }
        Configuration configuration = new Configuration();
        JSONArray configMap = sourceInfo.getJSONArray("configMap");
        List<SourceInfo> sourceInfoList = new ArrayList<>();
        for(int i = 0; i < configMap.size(); i++){
            SourceInfo e = configMap.getObject(i, SourceInfo.class);
            configuration.set(e.getName(), e.getValue());               // 配置多个ip
            sourceInfoList.add(new SourceInfo(e.getName(), e.getValue()));
        }
        log.info(sourceInfoList.toString());
        Connection connection = ConnectionManager.createConnection(sourceInfo.getString("sourceName"), configuration);
        if(connection == null){
            return Result.fail();
        }
        Boolean ans = hdsService.insertOne(sourceInfo.getString("sourceName"), JSONObject.toJSONString(sourceInfoList));
        if (!ans){
            Result.fail();
        }
        Integer id = hdsService.selectDataIdByName(sourceInfo.getString("sourceName"));
        hbaseManageService.insertOne(id, userId);
        return Result.ok();
    }

    /**
     * @Author Shawn Yue
     * @Description // 根据userId查询所有数据源名称
     *              // 首先根据userId查询mysql管理表找到数据源id的数组
     *              // 再根据id数组查询mysql数据源表得到数据源名称数组并返回
     * @Date 22:49 2022/10/2
     * @Param [userId]
     **/
    @GetMapping("/auth/getDataSource")
    public Result getDataSource(@RequestHeader("userId") Integer userId) {
        List<Integer> dataIdList = hbaseManageService.selectDataIdListByUserId(userId);
        List<String> sourceName = hdsService.selectDataNameListByDataIdList(dataIdList);
        return Result.ok(sourceName);
    }
    /**
     * @Author Shawn Yue
     * @Description // 创建新的命名空间
     *              // 在hbase中创建新的命名空间
     * @Date 15:57 2022/9/27
     * @Param [namespace, sourceName]
     **/
    @PostMapping("/auth/createNamespace")
    public Result createNamespace(@RequestBody JSONObject namespaceInfo,
//                                  @RequestParam("sourceName") String sourceName,
                                  @RequestHeader("role") String role) throws IOException {
        if(role.equals("user")){
            return Result.build(null, PERMISSION);
        }
        Connection connection = ConnectionManager.getConnection(namespaceInfo.getString("sourceName"));
        HBaseDDL.createNamespace(namespaceInfo.getString("namespace"), connection); // 2.2 给命令空间添加需求 builder.addConfiguration("user","hadoop");
        return Result.ok();
    }

    /**
     * @Author Shawn Yue
     * @Description // 根据数据源打印所有命名空间
     *              // 根据dataId查询到所有命名空间
     * @Date 19:42 2022/9/26
     * @param sourceName
     * @return
     **/
    @GetMapping("/auth/getNamespace")
    public Result getNamespace(@RequestParam("sourceName") String sourceName) throws IOException {
        Connection connection = ConnectionManager.getConnection(sourceName);
        if(connection == null){
            connection = get.getConnectionFromSql(sourceName);
        }
        String[] namespace = HBaseDDL.getNamespace(connection);
        if(namespace == null){
            return Result.fail(DATA_ERROR.getMessage());
        }
        return Result.ok(namespace);
    }

    /**
     * @Author Shawn Yue
     * @Description // 删除数据源
     *              // 先删除hbase中的数据源
     *              // 再删除mysql中管理表、数据源表的数据
     * @Date 15:34 2022/9/27
     * @param sourceName
     * @return
     **/
    @PostMapping("/auth/delSourceData")
    public Result delSourceData(@RequestBody JSONObject sourceName,
                                @RequestHeader("role") String role){
        if(role.equals("user")){
            return Result.build(null, PERMISSION);
        }
        ConnectionManager.closeConnection(sourceName.getString("sourceName"));
        Integer id = hdsService.selectDataIdByName(sourceName.getString("sourceName"));
        hdsService.delSourceData(id);
        hbaseManageService.delSourceData(id);
        return Result.ok();
    }
}

