package edu.wit.hbasemanager.manage.manageservice.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import edu.wit.hbasemanager.manage.manageservice.entity.HbaseDataSourceConfiguration;
import edu.wit.hbasemanager.manage.manageservice.entity.HbaseManage;
import edu.wit.hbasemanager.manage.manageservice.mapper.HbaseDataSourceConfigurationMapper;
import edu.wit.hbasemanager.manage.manageservice.mapper.HbaseManageMapper;
import edu.wit.hbasemanager.manage.manageservice.service.HbaseDataSourceConfigurationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-09-25
 */
@Service
@Slf4j
public class HbaseDataSourceConfigurationServiceImpl extends ServiceImpl<HbaseDataSourceConfigurationMapper, HbaseDataSourceConfiguration> implements HbaseDataSourceConfigurationService {
    @Autowired
    private HbaseManageMapper hbaseManageMapper;
    @Override
    public String selectDataNameByDataId(Integer dataId) {
        QueryWrapper<HbaseDataSourceConfiguration> qw = new QueryWrapper<>();
        HbaseDataSourceConfiguration id = baseMapper.selectOne(qw.eq("id", dataId));
        return id.getName();
    }

    @Override
    public Boolean insertOne(String sourceName, String ip) {
        QueryWrapper<HbaseDataSourceConfiguration> qw = new QueryWrapper<>();
        HbaseDataSourceConfiguration hdsConfig = new HbaseDataSourceConfiguration();
        hdsConfig.setName(sourceName);
        hdsConfig.setProperties(ip);
        int insert = baseMapper.insert(hdsConfig);
        return (insert == 1) ;
    }

    @Override
    public Integer selectDataIdByName(String sourceName) {
        QueryWrapper<HbaseDataSourceConfiguration> qw = new QueryWrapper<>();
        HbaseDataSourceConfiguration one = baseMapper.selectOne(qw.eq("name", sourceName));
        return one.getId();
    }

    @Override
    public void delSourceData(Integer dataId) {
        QueryWrapper<HbaseDataSourceConfiguration> qw = new QueryWrapper<>();
        baseMapper.delete(qw.eq("id", dataId));
    }

    @Override
    public List<String> selectDataNameListByDataIdList(List<Integer> dataIdList) {
        QueryWrapper<HbaseDataSourceConfiguration> qw = new QueryWrapper<>();
        List<String> dataNameList = new ArrayList<>();
        for (Integer i : dataIdList){
            HbaseDataSourceConfiguration id = baseMapper.selectOne(qw.eq("id", i));
            dataNameList.add(id.getName());
            qw.clear();
        }
        return dataNameList;
    }

    @Override
    public String selectPropertyBySourceName(String sourceName) {
        QueryWrapper<HbaseDataSourceConfiguration> qw = new QueryWrapper<>();
        log.info(sourceName);
        HbaseDataSourceConfiguration one = baseMapper.selectOne(qw.eq("name", sourceName));
        if (one == null){
            return null;
        }
        log.info(one.getName());
        log.info(one.getProperties());
        return one.getProperties();
    }

//    @Override
//    public List<String> getDataSourceById(Integer userId) {
//        QueryWrapper<HbaseManage> wrapper = new QueryWrapper<>();
//        List<HbaseManage> hbaseManageList = hbaseManageMapper.selectList(wrapper.eq("user_id", userId));
//        List<Integer> datasourceIdList = new ArrayList<>();
//        for (HbaseManage manage:hbaseManageList) {
//            datasourceIdList.add(manage.getDatasourceId());
//        }
//        List<String> datasourceNameList = new ArrayList<>();
//        for (Integer id:datasourceIdList) {
//            baseMapper.
//        }
//
//    }
}
