package edu.wit.hbasemanager.manage.manageservice.controller;


import edu.wit.hbasemanager.common.result.Result;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.ConnectionManager;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.HBaseDDL;
import edu.wit.hbasemanager.manage.manageservice.feign.DbFeignClient;
import org.apache.hadoop.hbase.client.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static edu.wit.hbasemanager.common.result.ResultCodeEnum.PERMISSION;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 作者
 * @since 2022-09-25
 */
@RestController
@RequestMapping("/api/manage/hbaseManage")
public class HbaseManageController {
    @Autowired
    DbFeignClient dbClient;

    /**
     * @Author Shawn Yue
     * @Description // 删除命名空间
     * @Date 15:58 2022/9/27
     * @Param [sourceName, namespace]
     **/
    @GetMapping("/auth/delNamespace")
    public Result delNamespace(@RequestParam("sourceName") String sourceName,
                               @RequestParam("namespace") String namespace,
                               @RequestHeader("role") String role) throws IOException {
        if(role.equals("user")){
            return Result.build(null, PERMISSION);
        }
        Connection connection = ConnectionManager.getConnection(sourceName);
        HBaseDDL.delNamespace(namespace, connection);
        return Result.ok();
    }

}

