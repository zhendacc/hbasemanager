package edu.wit.hbasemanager.manage.manageservice.service;

import edu.wit.hbasemanager.manage.manageservice.entity.HbaseUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 作者
 * @since 2022-09-25
 */
public interface HbaseUserService extends IService<HbaseUser> {
}
