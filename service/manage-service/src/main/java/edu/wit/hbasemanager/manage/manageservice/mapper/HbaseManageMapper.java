package edu.wit.hbasemanager.manage.manageservice.mapper;

import edu.wit.hbasemanager.manage.manageservice.entity.HbaseManage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author Shawn Yue
 * @Description //TODO Shawn Yue
 * @Date 18:39 2022/9/26
 * @Param
 * @return
 **/
@Mapper
public interface HbaseManageMapper extends BaseMapper<HbaseManage> {
}
