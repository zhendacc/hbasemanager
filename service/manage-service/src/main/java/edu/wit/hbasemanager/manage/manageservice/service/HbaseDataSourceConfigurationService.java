package edu.wit.hbasemanager.manage.manageservice.service;

import edu.wit.hbasemanager.manage.manageservice.entity.HbaseDataSourceConfiguration;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 作者
 * @since 2022-09-25
 */
public interface HbaseDataSourceConfigurationService extends IService<HbaseDataSourceConfiguration> {
    String selectDataNameByDataId(Integer dataId);
    Boolean insertOne(String sourceName, String ip);
    Integer selectDataIdByName(String sourceName);
    void delSourceData(Integer dataId);
    List<String> selectDataNameListByDataIdList(List<Integer> dataIdList);
    String selectPropertyBySourceName(String sourceName);
}
