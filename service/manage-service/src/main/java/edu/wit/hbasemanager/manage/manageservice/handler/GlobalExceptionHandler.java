package edu.wit.hbasemanager.manage.manageservice.handler;


import edu.wit.hbasemanager.common.result.Result;
import edu.wit.hbasemanager.common.result.ResultCodeEnum;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Throwable.class)
    public Result handleException(Throwable e) {
        e.printStackTrace();
       return Result.build(null, ResultCodeEnum.UNKNOWN_ERROR);
    }
}
