package edu.wit.hbasemanager.manage.manageservice.mapper;

import edu.wit.hbasemanager.manage.manageservice.entity.HbaseUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 作者
 * @since 2022-09-25
 */
@Mapper
public interface HbaseUserMapper extends BaseMapper<HbaseUser> {

}
