package edu.wit.hbasemanager.manage.manageservice.controller;

import com.alibaba.fastjson.JSONObject;
import edu.wit.hbasemanager.common.result.Result;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.ConnectionGet;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.ConnectionManager;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.HBaseDDL;
import edu.wit.hbasemanager.manage.manageservice.HBaseTool.HBaseDML;
import edu.wit.hbasemanager.manage.manageservice.entity.HbaseDataSourceConfiguration;
import edu.wit.hbasemanager.manage.manageservice.entity.HbaseManage;
import edu.wit.hbasemanager.manage.manageservice.feign.DbFeignClient;
import edu.wit.hbasemanager.manage.manageservice.service.HbaseDataSourceConfigurationService;
import edu.wit.hbasemanager.manage.manageservice.service.HbaseManageService;
import edu.wit.hbasemanager.manage.manageservice.service.HbaseUserService;
import edu.wit.hbasemanager.model.GetTableVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.data.Json;
import org.apache.hadoop.hbase.client.Connection;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.wit.hbasemanager.common.result.ResultCodeEnum.PERMISSION;

/**
 * @Classname HbaseTableController
 * @Description TODO
 * @Author Shawn Yue
 * @Date 18:18
 * @Version 1.0
 **/

@RestController
@Slf4j
@RequestMapping("/api/manage/HbaseTable")
public class HbaseTableController {
    @Autowired
    DbFeignClient dbClient;
    @Autowired
    HbaseManageService hbaseManageService;
    @Autowired
    HbaseDataSourceConfigurationService hdsService;
    @Autowired
    ConnectionGet get;

    /**
     * @Author Shawn Yue
     * @Description // 创建hbase新表
     *              // 根据userid查询mysql找到数据源名称数组,
     *              // 与传入的namespace进行比对查询是否一致，
     *              // 若一致则新建表
     * @Date 15:59 2022/9/27
     * @Param [tableName, family, userId, namespace, sourceName]
     **/
    @PostMapping("/auth/createTable")
    public Result createTable(@RequestBody JSONObject tableInfo,
                              @RequestHeader("userId") Integer userId,
                              @RequestHeader("role") String role) throws IOException {
        if(role.equals("user")){
            return Result.build(null, PERMISSION);
        }
        List<HbaseManage> hbaseManages = hbaseManageService.selectByUserId(userId);
        String s = null;
        log.info("family: {}",tableInfo.toJSONString());
        List<String> familyList = tableInfo.getObject("family", List.class);
        String[] familyListString  = new String[familyList.size()];
        for (int i=0;i<familyListString.length;i++) {
            familyListString[i] = familyList.get(i);
        }
        Connection connection = ConnectionManager.getConnection(tableInfo.getString("sourceName"));
        if(connection == null){
            connection = get.getConnectionFromSql(tableInfo.getString("sourceName"));
        }
        log.info(hbaseManages.toString());
        for(HbaseManage v : hbaseManages){
            s = hdsService.selectDataNameByDataId(v.getDatasourceId());
            if(s.equals(tableInfo.getString("sourceName"))){
                HBaseDDL.createTable(tableInfo.getString("namespace"),
                                     tableInfo.getString("tableName"), familyListString, connection);     // 创建表
                return Result.ok();
            }
        }
        return Result.fail();
    }

    /**
     * @Author Shawn Yue
     * @Description // 根据命名空间查询所有表
     * @Date 20:44 2022/9/26
     * @Param [sourceName, namespace]
     * @return
     **/
    @GetMapping("/auth/getTableName")
    public Result getTableName(@RequestParam("sourceName") String sourceName,
                               @RequestParam("namespace") String namespace) throws IOException {
        Connection connection = ConnectionManager.getConnection(sourceName);
        if(connection == null){
            connection = get.getConnectionFromSql(sourceName);
            if (connection == null){
                return Result.fail();
            }
        }
        List<String> tableName = HBaseDDL.getTableName(connection, namespace);
        return Result.ok(tableName);
    }

    /**
     * @Author Shawn Yue
     * @Description // 显示表中所有数据
     *              // start默认为0 stop默认为10
     * @Date 21:00 2022/9/26
     * @Param [sourceName, namespace, startRow, stopRow, tableName]
     * @return
     **/
    @GetMapping("/auth/getTable")
    public Result getTable(@RequestParam("sourceName") String sourceName,
                           @RequestParam("namespace") String namespace,
                           @RequestParam("startRow") Integer startRow,
                           @RequestParam("stopRow") Integer stopRow,
                           @RequestParam("tableName") String tableName) throws IOException {
        Connection connection = ConnectionManager.getConnection(sourceName);
        if (connection == null){
            connection = get.getConnectionFromSql(sourceName);
        }
        List<GetTableVo> cellsList = HBaseDML.scanRows(namespace, tableName, startRow, stopRow, connection);
        log.info("cellList:"+cellsList.toString());
        log.info("cellList.size():"+String.valueOf(cellsList.size()));
        if (cellsList.size() == 0){
            Result.fail();
        }
        Map<String, Object> cells = new HashMap<>();
        cells.put("cells", cellsList);
        return Result.ok(cells);
    }

    /**
     * @Author Shawn Yue
     * @Description // 向表中插入数据
     * @Date 11:43 2022/9/27
     * @Param [sourceName, namespace, tableName, rowKey, columnFamily, columnName, value]
     * @return
     **/
    @PostMapping("/auth/insertValue")
    public Result insertValue(@RequestBody JSONObject insertInfo) throws IOException {
        Connection connection = ConnectionManager.getConnection(insertInfo.getString("sourceName"));
        if (connection == null){
            connection = get.getConnectionFromSql(insertInfo.getString("sourceName"));
        }
        log.info("json:"+insertInfo.toJSONString());
        HBaseDML.putCell(insertInfo.getString("namespace"),
                         insertInfo.getString("tableName"),
                         insertInfo.getInteger("rowKey"),
                         insertInfo.getString("columnFamily"),
                         insertInfo.getString("columnName"),
                         insertInfo.getString("value"), connection);
        return Result.ok();
    }

    /**
     * @Author Shawn Yue
     * @Description // 删除一行数据
     * @Date 15:54 2022/9/27
     * @Param [sourceName, namespace, tableName, rowKey]
     **/
    @GetMapping("/auth/delRowData")
    public Result delRowData(@RequestParam("sourceName") String sourceName,
                             @RequestParam("namespace") String namespace,
                             @RequestParam("tableName") String tableName,
                             @RequestParam("rowKey") Integer rowKey) throws IOException {
        Connection connection = ConnectionManager.getConnection(sourceName);
        HBaseDML.deleteRow(namespace, tableName, rowKey, connection);
        return Result.ok();
    }
    /**
     * @Author fangkun
     * @Description // 删除一张hbase表
     * @Date 15:53 2022/9/27
     * @Param [sourceName, namespace, tableName]
     **/
    @PostMapping("/auth/delTable")
    public Result delTable(@RequestBody JSONObject delTableInfo,
                           @RequestHeader("role") String role) throws IOException {
        if(role.equals("user")){
            return Result.build(null, PERMISSION);
        }
        Connection connection = ConnectionManager.getConnection(delTableInfo.getString("sourceName"));
        HBaseDDL.deleteTable(delTableInfo.getString("namespace"),
                             delTableInfo.getString("tableName"), connection);
        return Result.ok();
    }

    /**
     * @Author Shawn Yue
     * @Description // 得到表的所有列族名称
     * @Date 15:54 2022/9/27
     * @Param [sourceName, namespace, tableName]
     */
    @PostMapping("/auth/getFamilies")
    public Result getFamilies(@RequestBody JSONObject getFamiliesInfo) throws IOException {
        Connection connection = ConnectionManager.getConnection(getFamiliesInfo.getString("sourceName"));
        if (connection == null){
            connection = get.getConnectionFromSql(getFamiliesInfo.getString("sourceName"));
        }
        List<String> families = HBaseDML.getFamilies(getFamiliesInfo.getString("namespace"),
                                                     getFamiliesInfo.getString("tableName"), connection);
        return Result.ok(families);
    }
}
